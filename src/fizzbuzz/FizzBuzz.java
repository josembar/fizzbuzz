package fizzbuzz;

/**
 *
 * @author Jose
 */
public class FizzBuzz {

    public static void main(String[] args) {
        
        for(int i = 1; i <= 100; i++)
        {
            if(i == 1)
            {
                System.out.println(i);
            }
            else
            {
                boolean prime = true;
                for(int j = 2; j <= i/2; j++)
                {
                    //System.out.println("for prime: " + j);
                    if(i % j == 0)
                    {
                        prime = false;
                        break;
                    }
                }
                if(prime)
                {
                    System.out.println(i+ " Prime");
                }
                
                else
                {
                    if(i % 3 == 0 && i % 5 != 0)
                    {
                        System.out.println(i + " Fizz");
                    }
                    else if (i % 3 != 0 && i % 5 == 0)
                    {
                        System.out.println(i + " Buzz");
                    }
                    else if (i % 3 == 0 && i % 5 == 0)
                    {
                        System.out.println(i + " FizzBuzz");
                    }
                    else System.out.println(i);
                }
            }
        }
    }
}
